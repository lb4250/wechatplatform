/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_flexible_joinin.entity;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 我参与的活动Entity
 * @author zwz
 * @version 2018-10-16
 */
@Table(name="t_flexible_joinin", alias="a", columns={
		@Column(name="joininid", attrName="joininid", label="用户主键的id 这张表示用户参加活动的表", isPK=true),
		@Column(name="openid", attrName="openid", label="微信中的openid"),
		@Column(name="memberid", attrName="memberid", label="用户id"),
		@Column(name="flexibleid", attrName="flexibleid", label="活动id"),
		@Column(name="flexiblename", attrName="flexiblename", label="活动名称"),
		@Column(name="flexiblestatus", attrName="flexiblestatus", label="活动状态 ", comment="活动状态 (1进行中 0已结束)"),
		@Column(name="flexibleimage", attrName="flexibleimage", label="活动图片"),
		@Column(name="subtitle", attrName="subtitle", label="简短标题"),
		@Column(name="content", attrName="content", label="资料内容详情"),
		@Column(name="enddate", attrName="enddate", label="截止日期"),
		@Column(name="uploadimage1", attrName="uploadimage1", label="上传图片1"),
		@Column(name="uploadimage2", attrName="uploadimage2", label="上传图片2"),
		@Column(name="uploadimage3", attrName="uploadimage3", label="上传图片3"),
		@Column(name="uploadimage4", attrName="uploadimage4", label="上传图片4"),
		@Column(name="uploadimage5", attrName="uploadimage5", label="上传图片5"),
		@Column(name="uploadvideo1", attrName="uploadvideo1", label="上传视频1"),
		@Column(name="uploadvideo2", attrName="uploadvideo2", label="上传视频2"),
		@Column(name="uploadvideo3", attrName="uploadvideo3", label="上传视频3"),
		@Column(includeEntity=DataEntity.class),
	}, orderBy="a.update_date DESC"
)
public class TflexibleJoinin extends DataEntity<TflexibleJoinin> {
	
	private static final long serialVersionUID = 1L;
	private String joininid;		// 用户主键的id 这张表示用户参加活动的表
	private String openid;		// 微信中的openid
	private String memberid;		// 用户id
	private String flexibleid;		// 活动id
	private String flexiblename;		// 活动名称
	private String flexiblestatus;		// 活动状态 (1进行中 0已结束)
	private String flexibleimage;		// 活动图片
	private String subtitle;		// 简短标题
	private String content;		// 资料内容详情
	private Date enddate;		// 截止日期
	private String uploadimage1;		// 上传图片1
	private String uploadimage2;		// 上传图片2
	private String uploadimage3;		// 上传图片3
	private String uploadimage4;		// 上传图片4
	private String uploadimage5;		// 上传图片5
	private String uploadvideo1;		// 上传视频1
	private String uploadvideo2;		// 上传视频2
	private String uploadvideo3;		// 上传视频3
	
	public TflexibleJoinin() {
		this(null);
	}

	public TflexibleJoinin(String id){
		super(id);
	}
	
	public String getJoininid() {
		return joininid;
	}

	public void setJoininid(String joininid) {
		this.joininid = joininid;
	}
	
	@NotBlank(message="微信中的openid不能为空")
	@Length(min=0, max=50, message="微信中的openid长度不能超过 50 个字符")
	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}
	
	@NotBlank(message="用户id不能为空")
	@Length(min=0, max=120, message="用户id长度不能超过 120 个字符")
	public String getMemberid() {
		return memberid;
	}

	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}
	
	@NotBlank(message="活动id不能为空")
	@Length(min=0, max=120, message="活动id长度不能超过 120 个字符")
	public String getFlexibleid() {
		return flexibleid;
	}

	public void setFlexibleid(String flexibleid) {
		this.flexibleid = flexibleid;
	}
	
	@Length(min=0, max=255, message="活动名称长度不能超过 255 个字符")
	public String getFlexiblename() {
		return flexiblename;
	}

	public void setFlexiblename(String flexiblename) {
		this.flexiblename = flexiblename;
	}
	
	@Length(min=0, max=40, message="活动状态 长度不能超过 40 个字符")
	public String getFlexiblestatus() {
		return flexiblestatus;
	}

	public void setFlexiblestatus(String flexiblestatus) {
		this.flexiblestatus = flexiblestatus;
	}
	
	@Length(min=0, max=1024, message="活动图片长度不能超过 1024 个字符")
	public String getFlexibleimage() {
		return flexibleimage;
	}

	public void setFlexibleimage(String flexibleimage) {
		this.flexibleimage = flexibleimage;
	}
	
	@Length(min=0, max=255, message="简短标题长度不能超过 255 个字符")
	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	
	@Length(min=0, max=2048, message="资料内容详情长度不能超过 2048 个字符")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	
	@Length(min=0, max=1024, message="上传图片1长度不能超过 1024 个字符")
	public String getUploadimage1() {
		return uploadimage1;
	}

	public void setUploadimage1(String uploadimage1) {
		this.uploadimage1 = uploadimage1;
	}
	
	@Length(min=0, max=1024, message="上传图片2长度不能超过 1024 个字符")
	public String getUploadimage2() {
		return uploadimage2;
	}

	public void setUploadimage2(String uploadimage2) {
		this.uploadimage2 = uploadimage2;
	}
	
	@Length(min=0, max=1024, message="上传图片3长度不能超过 1024 个字符")
	public String getUploadimage3() {
		return uploadimage3;
	}

	public void setUploadimage3(String uploadimage3) {
		this.uploadimage3 = uploadimage3;
	}
	
	@Length(min=0, max=1024, message="上传图片4长度不能超过 1024 个字符")
	public String getUploadimage4() {
		return uploadimage4;
	}

	public void setUploadimage4(String uploadimage4) {
		this.uploadimage4 = uploadimage4;
	}
	
	@Length(min=0, max=1024, message="上传图片5长度不能超过 1024 个字符")
	public String getUploadimage5() {
		return uploadimage5;
	}

	public void setUploadimage5(String uploadimage5) {
		this.uploadimage5 = uploadimage5;
	}
	
	@Length(min=0, max=1024, message="上传视频1长度不能超过 1024 个字符")
	public String getUploadvideo1() {
		return uploadvideo1;
	}

	public void setUploadvideo1(String uploadvideo1) {
		this.uploadvideo1 = uploadvideo1;
	}
	
	@Length(min=0, max=1024, message="上传视频2长度不能超过 1024 个字符")
	public String getUploadvideo2() {
		return uploadvideo2;
	}

	public void setUploadvideo2(String uploadvideo2) {
		this.uploadvideo2 = uploadvideo2;
	}
	
	@Length(min=0, max=1024, message="上传视频3长度不能超过 1024 个字符")
	public String getUploadvideo3() {
		return uploadvideo3;
	}

	public void setUploadvideo3(String uploadvideo3) {
		this.uploadvideo3 = uploadvideo3;
	}
	
}