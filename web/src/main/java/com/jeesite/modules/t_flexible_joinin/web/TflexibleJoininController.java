/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_flexible_joinin.web;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.file.utils.FileUploadUtils;
import com.jeesite.modules.t_flexible_joinin.entity.TflexibleJoinin;
import com.jeesite.modules.t_flexible_joinin.service.TflexibleJoininService;
import com.jeesite.modules.t_wxmember.entity.Twxmember;
import com.jeesite.modules.t_wxmember.service.TwxmemberService;

/**
 * 我参与的活动Controller
 * @author zwz
 * @version 2018-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/t_flexible_joinin/tflexibleJoinin")
public class TflexibleJoininController extends BaseController {

//	SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM");
	
	private String upload_host_port = "192.168.1.198:80";
	
	@Autowired
	private TwxmemberService twxmemberService;
	
	@Autowired
	private TflexibleJoininService tflexibleJoininService;
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public TflexibleJoinin get(String joininid, boolean isNewRecord) {
		return tflexibleJoininService.get(joininid, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("t_flexible_joinin:tflexibleJoinin:view")
	@RequestMapping(value = {"list", ""})
	public String list(TflexibleJoinin tflexibleJoinin, Model model) {
		model.addAttribute("tflexibleJoinin", tflexibleJoinin);
		return "modules/t_flexible_joinin/tflexibleJoininList";
	}
	
	/**
	 * 查询列表数据
	 */
//	@RequiresPermissions("t_flexible_joinin:tflexibleJoinin:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<TflexibleJoinin> listData(TflexibleJoinin tflexibleJoinin, HttpServletRequest request, HttpServletResponse response) {
		Page<TflexibleJoinin> page = tflexibleJoininService.findPage(new Page<TflexibleJoinin>(request, response), tflexibleJoinin); 
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("t_flexible_joinin:tflexibleJoinin:view")
	@RequestMapping(value = "form")
	public String form(TflexibleJoinin tflexibleJoinin, Model model) {
		model.addAttribute("tflexibleJoinin", tflexibleJoinin);
		return "modules/t_flexible_joinin/tflexibleJoininForm";
	}

	
	
	/*
	@RequiresPermissions("t_flexible_joinin:tflexibleJoinin:view")
	@RequestMapping(value = "form")
	public String form(TflexibleJoinin tflexibleJoinin, Model model) {
		// 保存上传图片
		FileUploadUtils.saveFileUpload( tflexibleJoinin.getId(), "twxmember_image");
		// 保存上传附件
		FileUploadUtils.saveFileUpload(tflexibleJoinin.getId(), "twxmember_file");
		model.addAttribute("tflexibleJoinin", tflexibleJoinin);
		return "modules/t_flexible_joinin/tflexibleJoininForm";
	}
	 */
	
	
	
	/**
	 * 上传数据的接口
	 */
//	@RequiresPermissions("t_flexible_joinin:tflexibleJoinin:view")
	@RequestMapping(value = "/addt_flexible_joininFile",method = RequestMethod.POST)
    @ResponseBody
	public String addOrderComment(TflexibleJoinin tflexibleJoinin ,
                                          MultipartFile fimg1,
                                          MultipartFile fimg2,
                                          MultipartFile fimg3,
                                          MultipartFile fimg4,
                                          MultipartFile fimg5,
                                          MultipartFile fvideourl1,
                                          MultipartFile fvideourl2,
                                          MultipartFile fvideourl3
                                          ){
		
		//获取到微信的openid
		String openid = tflexibleJoinin.getOpenid();
		if( StringUtils.isEmpty(openid) ){
			return "openid为空";
		}
		
	    //根据 openid 查询出用户的id
        Twxmember twxmember = new Twxmember();
        twxmember.setOpenid(openid);
        List<Twxmember>twxmemberList = twxmemberService.findList( twxmember );
   
	
        String storeDir = "G:/uploadImages/";   //文件存储根路径        
        Date today = new Date();
        String year = String.valueOf(today.getYear()+1900);
        String month = String.valueOf(today.getMonth()+1 );
        String day = String.valueOf(today.getDay() );
        
        //使用openid_flexibleid  合并的方式来创建目录
        String returnfiledir = sdf.format( new Date() ) + "/openid" + twxmemberList.get(0).getOpenid()+"_flexibleid"+tflexibleJoinin.getFlexibleid() + "/";
        String filedir = storeDir + returnfiledir;
        
        
        File file = new File( filedir );
        if( !file.exists() )
            file.mkdirs();
        
        String createfile1 = "";
        String createfile2 = "";
        String createfile3 = "";
        String createfile4 = "";
        String createfile5 = "";
        String videofile1 = "";
        String videofile2 = "";
        String videofile3 = "";
        
        if( fimg1!=null && fimg1.getSize()>0 )
            createfile1 = filedir + UUID.randomUUID() + fimg1.getOriginalFilename().substring( fimg1.getOriginalFilename().lastIndexOf(".") );
        if( fimg2!=null && fimg2.getSize()>0 )
            createfile2 = filedir + UUID.randomUUID() + fimg2.getOriginalFilename().substring( fimg2.getOriginalFilename().lastIndexOf(".") );
        if( fimg3!=null && fimg3.getSize()>0 )
            createfile3 = filedir + UUID.randomUUID() + fimg3.getOriginalFilename().substring( fimg3.getOriginalFilename().lastIndexOf(".") );
        if( fimg4!=null && fimg4.getSize()>0 )
            createfile4 = filedir + UUID.randomUUID() + fimg4.getOriginalFilename().substring( fimg4.getOriginalFilename().lastIndexOf(".") );
        if( fimg5!=null && fimg5.getSize()>0 )
            createfile5 = filedir + UUID.randomUUID() + fimg5.getOriginalFilename().substring( fimg5.getOriginalFilename().lastIndexOf(".") );
        if( fvideourl1!=null && fvideourl1.getSize()>0 )
            videofile1 = filedir + UUID.randomUUID() + fvideourl1.getOriginalFilename().substring( fvideourl1.getOriginalFilename().lastIndexOf(".") );
        if( fvideourl2!=null && fvideourl2.getSize()>0 )
            videofile2 = filedir + UUID.randomUUID() + fvideourl2.getOriginalFilename().substring( fvideourl2.getOriginalFilename().lastIndexOf(".") );
        if( fvideourl3!=null && fvideourl3.getSize()>0 )
            videofile3 = filedir + UUID.randomUUID() + fvideourl3.getOriginalFilename().substring( fvideourl3.getOriginalFilename().lastIndexOf(".") );
        
        
        
        
        try {
            if( fimg1!=null && !StringUtils.isBlank(createfile1) )
                fimg1.transferTo( new File( createfile1 ) );
            if( fimg2!=null && !StringUtils.isBlank(createfile2) )
                fimg2.transferTo( new File( createfile2 ) );
            if( fimg3!=null && !StringUtils.isBlank(createfile3) )
                fimg3.transferTo( new File( createfile3 ) );
            if( fimg4!=null && !StringUtils.isBlank(createfile4) )
                fimg4.transferTo( new File( createfile4 ) );
            if( fimg5!=null && !StringUtils.isBlank(createfile5) )
                fimg5.transferTo( new File( createfile5 ) );
            
            if( fvideourl1!=null && !StringUtils.isBlank(videofile1) )
                fvideourl1.transferTo( new File( videofile1 ) );
            if( fvideourl2!=null && !StringUtils.isBlank(videofile2) )
                fvideourl2.transferTo( new File( videofile2 ) );
            if( fvideourl3!=null && !StringUtils.isBlank(videofile3) )
                fvideourl3.transferTo( new File( videofile3 ) );
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("内部错误!");
        }
    
        if( fimg1!=null && !StringUtils.isBlank(createfile1) )
        	tflexibleJoinin.setUploadimage1( "http://" + this.upload_host_port +   createfile1.substring( createfile1.indexOf("uploadImages")+ 12 ));
        if( fimg2!=null && !StringUtils.isBlank(createfile2) )
        	tflexibleJoinin.setUploadimage2( "http://" + this.upload_host_port +   createfile2.substring( createfile2.indexOf("uploadImages")+ 12 ));
        if( fimg3!=null && !StringUtils.isBlank(createfile3) )
        	tflexibleJoinin.setUploadimage3( "http://" + this.upload_host_port +   createfile3.substring( createfile3.indexOf("uploadImages")+ 12 ));
        if( fimg4!=null && !StringUtils.isBlank(createfile4) )
        	tflexibleJoinin.setUploadimage4( "http://" + this.upload_host_port +   createfile4.substring( createfile4.indexOf("uploadImages")+ 12 ));
        if( fimg5!=null && !StringUtils.isBlank(createfile5) )
        	tflexibleJoinin.setUploadimage5( "http://" + this.upload_host_port +   createfile5.substring( createfile5.indexOf("uploadImages")+ 12 ));
        
        if( fvideourl1!=null && !StringUtils.isBlank(videofile1) )
        	tflexibleJoinin.setUploadvideo1( "http://" + this.upload_host_port +   videofile1.substring( videofile1.indexOf("uploadImages")+ 12 ));
        if( fvideourl2!=null && !StringUtils.isBlank(videofile2) )
        	tflexibleJoinin.setUploadvideo1( "http://" + this.upload_host_port +   videofile2.substring( videofile2.indexOf("uploadImages")+ 12 ));
        if( fvideourl3!=null && !StringUtils.isBlank(videofile3) )
        	tflexibleJoinin.setUploadvideo1( "http://" + this.upload_host_port +   videofile3.substring( videofile3.indexOf("uploadImages")+ 12 ));
        	
    
        //设置到 memberid 上
        tflexibleJoinin.setCreateDate(new Date());
        tflexibleJoinin.setMemberid( twxmemberList.get(0).getMemberid() );
        
        tflexibleJoininService.insert( tflexibleJoinin  );
        return "upload success!";
        
        
    }
	
	
	
	
	
	/**
	 * 保存我参与的活动(JX)
	 */
	@RequiresPermissions("t_flexible_joinin:tflexibleJoinin:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated TflexibleJoinin tflexibleJoinin) {
		tflexibleJoininService.save(tflexibleJoinin);
		return renderResult(Global.TRUE, text("保存我参与的活动(JX)成功！"));
	}
	
	/**
	 * 停用我参与的活动(JX)
	 */
	@RequiresPermissions("t_flexible_joinin:tflexibleJoinin:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(TflexibleJoinin tflexibleJoinin) {
		tflexibleJoinin.setStatus(TflexibleJoinin.STATUS_DISABLE);
		tflexibleJoininService.updateStatus(tflexibleJoinin);
		return renderResult(Global.TRUE, text("停用我参与的活动(JX)成功"));
	}
	
	/**
	 * 启用我参与的活动(JX)
	 */
	@RequiresPermissions("t_flexible_joinin:tflexibleJoinin:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(TflexibleJoinin tflexibleJoinin) {
		tflexibleJoinin.setStatus(TflexibleJoinin.STATUS_NORMAL);
		tflexibleJoininService.updateStatus(tflexibleJoinin);
		return renderResult(Global.TRUE, text("启用我参与的活动(JX)成功"));
	}
	
	/**
	 * 删除我参与的活动(JX)
	 */
	@RequiresPermissions("t_flexible_joinin:tflexibleJoinin:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(TflexibleJoinin tflexibleJoinin) {
		tflexibleJoininService.delete(tflexibleJoinin);
		return renderResult(Global.TRUE, text("删除我参与的活动(JX)成功！"));
	}
	
}