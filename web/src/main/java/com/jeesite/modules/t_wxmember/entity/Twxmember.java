/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_wxmember.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 微信用户Entity
 * @author zwz
 * @version 2018-10-22
 */
@Table(name="t_wxmember", alias="a", columns={
		@Column(name="memberid", attrName="memberid", label="用户号的id", isPK=true),
		@Column(name="openid", attrName="openid", label="微信openid"),
		@Column(name="session_key", attrName="sessionKey", label="微信session_key"),
		@Column(name="code", attrName="code", label="微信的code"),
		@Column(name="wxmembername", attrName="wxmembername", label="微信用户名"),
		@Column(name="wxphoto", attrName="wxphoto", label="微信头像"),
		@Column(name="loginstatus", attrName="loginstatus", label="登录状态 1表示已经登录 0表示未登录"),
		@Column(name="phone", attrName="phone", label="手机号"),
		@Column(name="alipaynum", attrName="alipaynum", label="支付宝号码"),
		@Column(name="microblog", attrName="microblog", label="微博号码"),
		@Column(name="qq", attrName="qq", label="用户的qq号码"),
		@Column(name="mobile", attrName="mobile", label="电话"),
		@Column(name="email", attrName="email", label="邮箱"),
		@Column(name="companyemail", attrName="companyemail", label="公司邮箱"),
		@Column(name="sex", attrName="sex", label="性别"),
		@Column(name="username", attrName="username", label="用户名"),
		@Column(name="password", attrName="password", label="密码"),
		@Column(name="department", attrName="department", label="所在部门"),
		@Column(name="worklocation", attrName="worklocation", label="工作地点"),
		@Column(name="last_login_ip", attrName="lastLoginIp", label="上次登录ip"),
		@Column(name="last_login_date", attrName="lastLoginDate", label="最后登录时间"),
		@Column(includeEntity=DataEntity.class),
		@Column(name="sign", attrName="sign", label="个性签名"),
		@Column(name="img", attrName="img", label="图片0"),
		@Column(name="img1", attrName="img1", label="图片1"),
		@Column(name="img2", attrName="img2", label="图片2"),
		@Column(name="videourl", attrName="videourl", label="视频路径"),
		@Column(name="extend1", attrName="extend1", label="扩展1"),
		@Column(name="extend2", attrName="extend2", label="扩展2"),
		@Column(name="extend3", attrName="extend3", label="扩展3"),
	}, orderBy="a.update_date DESC"
)
public class Twxmember extends DataEntity<Twxmember> {
	
	private static final long serialVersionUID = 1L;
	private String memberid;		// 用户号的id
	private String openid;		// 微信openid
	private String sessionKey;		// 微信session_key
	private String code;		// 微信的code
	private String wxmembername;		// 微信用户名
	private String wxphoto;		// 微信头像
	private String loginstatus;		// 登录状态 1表示已经登录 0表示未登录
	private String phone;		// 手机号
	private String alipaynum;		// 支付宝号码
	private String microblog;		// 微博号码
	private String qq;		// 用户的qq号码
	private String mobile;		// 电话
	private String email;		// 邮箱
	private String companyemail;		// 公司邮箱
	private String sex;		// 性别
	private String username;		// 用户名
	private String password;		// 密码
	private String department;		// 所在部门
	private String worklocation;		// 工作地点
	private String lastLoginIp;		// 上次登录ip
	private Date lastLoginDate;		// 最后登录时间
	private String sign;		// 个性签名
	private String img;		// 图片0
	private String img1;		// 图片1
	private String img2;		// 图片2
	private String videourl;		// 视频路径
	private String extend1;		// 扩展1
	private String extend2;		// 扩展2
	private String extend3;		// 扩展3
	
	public Twxmember() {
		this(null);
	}

	public Twxmember(String id){
		super(id);
	}
	
	public String getMemberid() {
		return memberid;
	}

	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}
	
	@Length(min=0, max=128, message="微信openid长度不能超过 128 个字符")
	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}
	
	@Length(min=0, max=128, message="微信session_key长度不能超过 128 个字符")
	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
	
	@Length(min=0, max=128, message="微信的code长度不能超过 128 个字符")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@Length(min=0, max=255, message="微信用户名长度不能超过 255 个字符")
	public String getWxmembername() {
		return wxmembername;
	}

	public void setWxmembername(String wxmembername) {
		this.wxmembername = wxmembername;
	}
	
	@Length(min=0, max=255, message="微信头像长度不能超过 255 个字符")
	public String getWxphoto() {
		return wxphoto;
	}

	public void setWxphoto(String wxphoto) {
		this.wxphoto = wxphoto;
	}
	
	@Length(min=0, max=2, message="登录状态 1表示已经登录 0表示未登录长度不能超过 2 个字符")
	public String getLoginstatus() {
		return loginstatus;
	}

	public void setLoginstatus(String loginstatus) {
		this.loginstatus = loginstatus;
	}
	
	@Length(min=0, max=11, message="手机号长度不能超过 11 个字符")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Length(min=0, max=20, message="支付宝号码长度不能超过 20 个字符")
	public String getAlipaynum() {
		return alipaynum;
	}

	public void setAlipaynum(String alipaynum) {
		this.alipaynum = alipaynum;
	}
	
	@Length(min=0, max=20, message="微博号码长度不能超过 20 个字符")
	public String getMicroblog() {
		return microblog;
	}

	public void setMicroblog(String microblog) {
		this.microblog = microblog;
	}
	
	@Length(min=0, max=20, message="用户的qq号码长度不能超过 20 个字符")
	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}
	
	@Length(min=0, max=20, message="电话长度不能超过 20 个字符")
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	@Length(min=0, max=40, message="邮箱长度不能超过 40 个字符")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Length(min=0, max=40, message="公司邮箱长度不能超过 40 个字符")
	public String getCompanyemail() {
		return companyemail;
	}

	public void setCompanyemail(String companyemail) {
		this.companyemail = companyemail;
	}
	
	@Length(min=0, max=4, message="性别长度不能超过 4 个字符")
	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
	
	@Length(min=0, max=40, message="用户名长度不能超过 40 个字符")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@Length(min=0, max=40, message="密码长度不能超过 40 个字符")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Length(min=0, max=30, message="所在部门长度不能超过 30 个字符")
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	
	@Length(min=0, max=40, message="工作地点长度不能超过 40 个字符")
	public String getWorklocation() {
		return worklocation;
	}

	public void setWorklocation(String worklocation) {
		this.worklocation = worklocation;
	}
	
	@Length(min=0, max=100, message="上次登录ip长度不能超过 100 个字符")
	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}
	
	@Length(min=0, max=255, message="个性签名长度不能超过 255 个字符")
	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}
	
	@Length(min=0, max=1024, message="图片0长度不能超过 1024 个字符")
	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}
	
	@Length(min=0, max=1024, message="图片1长度不能超过 1024 个字符")
	public String getImg1() {
		return img1;
	}

	public void setImg1(String img1) {
		this.img1 = img1;
	}
	
	@Length(min=0, max=1024, message="图片2长度不能超过 1024 个字符")
	public String getImg2() {
		return img2;
	}

	public void setImg2(String img2) {
		this.img2 = img2;
	}
	
	@Length(min=0, max=1024, message="视频路径长度不能超过 1024 个字符")
	public String getVideourl() {
		return videourl;
	}

	public void setVideourl(String videourl) {
		this.videourl = videourl;
	}
	
	@Length(min=0, max=255, message="扩展1长度不能超过 255 个字符")
	public String getExtend1() {
		return extend1;
	}

	public void setExtend1(String extend1) {
		this.extend1 = extend1;
	}
	
	@Length(min=0, max=255, message="扩展2长度不能超过 255 个字符")
	public String getExtend2() {
		return extend2;
	}

	public void setExtend2(String extend2) {
		this.extend2 = extend2;
	}
	
	@Length(min=0, max=255, message="扩展3长度不能超过 255 个字符")
	public String getExtend3() {
		return extend3;
	}

	public void setExtend3(String extend3) {
		this.extend3 = extend3;
	}
	
}