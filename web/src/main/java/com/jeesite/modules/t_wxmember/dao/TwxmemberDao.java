/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_wxmember.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.t_wxmember.entity.Twxmember;

/**
 * 微信用户DAO接口
 * @author zwz
 * @version 2018-10-22
 */
@MyBatisDao
public interface TwxmemberDao extends CrudDao<Twxmember> {
	
}