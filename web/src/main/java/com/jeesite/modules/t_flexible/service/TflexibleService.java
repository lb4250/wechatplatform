/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_flexible.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.t_flexible.entity.Tflexible;
import com.jeesite.modules.t_flexible.dao.TflexibleDao;
import com.jeesite.modules.file.utils.FileUploadUtils;

/**
 * 发起的活动Service
 * @author zwz
 * @version 2018-10-20
 */
@Service
@Transactional(readOnly=true)
public class TflexibleService extends CrudService<TflexibleDao, Tflexible> {
	
	/**
	 * 获取单条数据
	 * @param tflexible
	 * @return
	 */
	@Override
	public Tflexible get(Tflexible tflexible) {
		return super.get(tflexible);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param tflexible
	 * @return
	 */
	@Override
	public Page<Tflexible> findPage(Page<Tflexible> page, Tflexible tflexible) {
		return super.findPage(page, tflexible);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param tflexible
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Tflexible tflexible) {
		super.save(tflexible);
		// 保存上传图片
		FileUploadUtils.saveFileUpload(tflexible.getId(), "tflexible_image");
		// 保存上传附件
		FileUploadUtils.saveFileUpload(tflexible.getId(), "tflexible_file");
	}
	
	/**
	 * 更新状态
	 * @param tflexible
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Tflexible tflexible) {
		super.updateStatus(tflexible);
	}
	
	/**
	 * 删除数据
	 * @param tflexible
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Tflexible tflexible) {
		super.delete(tflexible);
	}
	
}