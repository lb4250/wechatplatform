/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_information.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.frontEntity.IndexPage;
import com.jeesite.modules.t_collect_information.entity.TcollectInformation;
import com.jeesite.modules.t_collect_information.service.TcollectInformationService;
import com.jeesite.modules.t_indexbanner.entity.Tindexbanner;
import com.jeesite.modules.t_indexbanner.service.TindexbannerService;
import com.jeesite.modules.t_information.entity.Tinformation;
import com.jeesite.modules.t_information.service.TinformationService;

/**
 * 系统资讯Controller
 * @author zwz
 * @version 2018-10-18
 * 
 * 注意这个生成类有修改过，去掉了权限注释
 * 
 * 
 */
@Controller
@RequestMapping(value = "${adminPath}/t_information/tinformation")
public class TinformationController extends BaseController {

	@Autowired
	private TinformationService tinformationService;
	
	@Autowired
	private TindexbannerService tindexbannerService;
	
	@Autowired
	private TcollectInformationService tcollectInformationService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Tinformation get(String infoid, boolean isNewRecord) {
		return tinformationService.get(infoid, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
//	@RequiresPermissions("t_information:tinformation:view")
	@RequestMapping(value = {"list", ""})
	public String list(Tinformation tinformation, Model model) {
		model.addAttribute("tinformation", tinformation);
		return "modules/t_information/tinformationList";
	}
	
	
	/**
	 * 查询推荐列表数据
	 */
//	@RequiresPermissions("t_information:tinformation:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Tinformation> listData(Tinformation tinformation, HttpServletRequest request, HttpServletResponse response) {
		
		String openid = request.getParameter("openid");
		
		Page<Tinformation> page = tinformationService.findPage(new Page<Tinformation>(request, response), tinformation); 
		//设置是否收藏的状态
		for( int i=0;i<page.getList().size();i++ ){
			TcollectInformation tcollectInformation	= new TcollectInformation();
		    tcollectInformation.setInfoid( page.getList().get(i).getInfoid() );
			//设置收藏次数
		    page.getList().get(i).setCollectcount( new Long(  tcollectInformationService.findCount( tcollectInformation )).toString() );
		    //设置个人是否收藏
		    tcollectInformation.setOpenid(openid);
		    page.getList().get(i).setIscollect(  tcollectInformationService.findCount( tcollectInformation )>0?"1":"0"  ); 
		    
		}
		return page;
		
	}

	
	/**
	 * 查询出推荐的列表信息以及后面的 banner 信息
	 */
	@RequestMapping(value = "indexPage")
	@ResponseBody
	public IndexPage indexPage(Tinformation tinformation, HttpServletRequest request, HttpServletResponse response) {
		
		String openid = request.getParameter("openid");
		
		tinformation.setIsrecommend(1);  //设置成推荐状态1
		Page<Tinformation> pg = new Page<Tinformation>(request, response);
		pg.setPageNo(  Integer.parseInt(request.getParameter("pageNo"))  );
		pg.setPageSize(  Integer.parseInt( request.getParameter("pageSize") )  );
		
//		Page<Tinformation> page = tinformationService.findPage(new Page<Tinformation>(request, response), tinformation); 
		Page<Tinformation> page = tinformationService.findPage( pg , tinformation); 
		//设置是否收藏的状态
		for( int i=0;i<page.getList().size();i++ ){
			TcollectInformation tcollectInformation	= new TcollectInformation();
		    tcollectInformation.setInfoid( page.getList().get(i).getInfoid() );
			//设置收藏次数
		    page.getList().get(i).setCollectcount( new Long(  tcollectInformationService.findCount( tcollectInformation )).toString() );
		    //设置个人是否收藏
		    tcollectInformation.setOpenid(openid);
		    page.getList().get(i).setIscollect(  tcollectInformationService.findCount( tcollectInformation )>0?"1":"0"  ); 
		    
		}
		
		Tindexbanner tindexbanner = new Tindexbanner();
		tindexbanner.setStatus("0");  //查询正常状态的banner
		
		List<Tindexbanner>tindexbannerList = tindexbannerService.findList( tindexbanner );
		IndexPage indexPage = new IndexPage( page.getList() , tindexbannerList);
		return indexPage;
		
		
	}
	
	
	
	
	
	
	/**
	 * 查看编辑表单
	 */
//	@RequiresPermissions("t_information:tinformation:view")
	@RequestMapping(value = "form")
	public String form(Tinformation tinformation, Model model) {
		model.addAttribute("tinformation", tinformation);
		return "modules/t_information/tinformationForm";
	}
	
	
	/**
	 * 保存t_information
	 */
//	@RequiresPermissions("t_information:tinformation:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Tinformation tinformation) {
		tinformationService.save(tinformation);
		return renderResult(Global.TRUE, text("保存t_information成功！"));
	}
	
	/**
	 * 停用t_information
	 */
//	@RequiresPermissions("t_information:tinformation:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(Tinformation tinformation) {
		tinformation.setStatus(Tinformation.STATUS_DISABLE);
		tinformationService.updateStatus(tinformation);
		return renderResult(Global.TRUE, text("停用t_information成功"));
	}
	
	/**
	 * 启用t_information
	 */
//	@RequiresPermissions("t_information:tinformation:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(Tinformation tinformation) {
		tinformation.setStatus(Tinformation.STATUS_NORMAL);
		tinformationService.updateStatus(tinformation);
		return renderResult(Global.TRUE, text("启用t_information成功"));
	}
	
	/**
	 * 删除t_information
	 */
//	@RequiresPermissions("t_information:tinformation:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Tinformation tinformation) {
		tinformationService.delete(tinformation);
		return renderResult(Global.TRUE, text("删除t_information成功！"));
	}
	
	
	
}