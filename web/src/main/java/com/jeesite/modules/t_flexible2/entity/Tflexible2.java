/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_flexible2.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;
import com.jeesite.common.collect.ListUtils;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 发起的活动Entity
 * @author zwz
 * @version 2018-10-20
 */
@Table(name="t_flexible", alias="a", columns={
		@Column(name="flexibleid", attrName="flexibleid", label="活动id", isPK=true),
		@Column(name="flexiblename", attrName="flexiblename", label="活动名称"),
		@Column(name="title", attrName="title", label="活动主题"),
		@Column(name="subject", attrName="subject", label="活动主旨"),
		@Column(name="illustrate", attrName="illustrate", label="活动说明"),
		@Column(name="createdate", attrName="createdate", label="createdate"),
		@Column(name="content", attrName="content", label="活动内容"),
		@Column(name="begindate", attrName="begindate", label="开始时间", queryType=QueryType.GTE),
		@Column(name="enddate", attrName="enddate", label="结束时间", queryType=QueryType.LTE),
		@Column(name="joininway", attrName="joininway", label="参与方式"),
		@Column(name="address", attrName="address", label="活动地址"),
		@Column(name="flexiablelimit", attrName="flexiablelimit", label="活动限制"),
		@Column(name="flexiblestatus", attrName="flexiblestatus", label="活动状态"),
		@Column(name="img", attrName="img", label="图片0"),
		@Column(name="img1", attrName="img1", label="图片1"),
		@Column(name="img2", attrName="img2", label="图片2"),
		@Column(name="ico", attrName="ico", label="ico图片"),
		@Column(name="flexibledetail", attrName="flexibledetail", label="活动说明"),
		@Column(name="extend1", attrName="extend1", label="扩展1"),
		@Column(name="extend2", attrName="extend2", label="扩展2"),
		@Column(name="extend3", attrName="extend3", label="扩展3"),
		@Column(name="detailurl", attrName="detailurl", label="点击之后跳转的路径"),
		@Column(name="praisecount", attrName="praisecount", label="点赞数量"),
		@Column(name="spitslotcount", attrName="spitslotcount", label="吐槽数量"),
		@Column(name="commentcount", attrName="commentcount", label="评价数量"),
		@Column(includeEntity=DataEntity.class),
	}, orderBy="a.update_date DESC"
)
public class Tflexible2 extends DataEntity<Tflexible2> {
	
	private static final long serialVersionUID = 1L;
	private String flexibleid;		// 活动id
	private String flexiblename;		// 活动名称
	private String title;		// 活动主题
	private String subject;		// 活动主旨
	private String illustrate;		// 活动说明
	private Date createdate;		// createdate
	private String content;		// 活动内容
	private Date begindate;		// 开始时间
	private Date enddate;		// 结束时间
	private String joininway;		// 参与方式
	private String address;		// 活动地址
	private String flexiablelimit;		// 活动限制
	private String flexiblestatus;		// 活动状态
	private String img;		// 图片0
	private String img1;		// 图片1
	private String img2;		// 图片2
	private String ico;		// ico图片
	private String flexibledetail;		// 活动说明
	private String extend1;		// 扩展1
	private String extend2;		// 扩展2
	private String extend3;		// 扩展3
	private String detailurl;		// 点击之后跳转的路径
	private Long praisecount;		// 点赞数量
	private Long spitslotcount;		// 吐槽数量
	private Long commentcount;		// 评价数量
	private List<TflexibleJoinin2> tflexibleJoinin2List = ListUtils.newArrayList();		// 子表列表
	
	public Tflexible2() {
		this(null);
	}

	public Tflexible2(String id){
		super(id);
	}
	
	public String getFlexibleid() {
		return flexibleid;
	}

	public void setFlexibleid(String flexibleid) {
		this.flexibleid = flexibleid;
	}
	
	@Length(min=0, max=255, message="活动名称长度不能超过 255 个字符")
	public String getFlexiblename() {
		return flexiblename;
	}

	public void setFlexiblename(String flexiblename) {
		this.flexiblename = flexiblename;
	}
	
	@Length(min=0, max=255, message="活动主题长度不能超过 255 个字符")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@Length(min=0, max=255, message="活动主旨长度不能超过 255 个字符")
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	@Length(min=0, max=255, message="活动说明长度不能超过 255 个字符")
	public String getIllustrate() {
		return illustrate;
	}

	public void setIllustrate(String illustrate) {
		this.illustrate = illustrate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	
	@Length(min=0, max=1024, message="活动内容长度不能超过 1024 个字符")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getBegindate() {
		return begindate;
	}

	public void setBegindate(Date begindate) {
		this.begindate = begindate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	
	@Length(min=0, max=255, message="参与方式长度不能超过 255 个字符")
	public String getJoininway() {
		return joininway;
	}

	public void setJoininway(String joininway) {
		this.joininway = joininway;
	}
	
	@Length(min=0, max=255, message="活动地址长度不能超过 255 个字符")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Length(min=0, max=255, message="活动限制长度不能超过 255 个字符")
	public String getFlexiablelimit() {
		return flexiablelimit;
	}

	public void setFlexiablelimit(String flexiablelimit) {
		this.flexiablelimit = flexiablelimit;
	}
	
	@Length(min=0, max=10, message="活动状态长度不能超过 10 个字符")
	public String getFlexiblestatus() {
		return flexiblestatus;
	}

	public void setFlexiblestatus(String flexiblestatus) {
		this.flexiblestatus = flexiblestatus;
	}
	
	@Length(min=0, max=1024, message="图片0长度不能超过 1024 个字符")
	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}
	
	@Length(min=0, max=1024, message="图片1长度不能超过 1024 个字符")
	public String getImg1() {
		return img1;
	}

	public void setImg1(String img1) {
		this.img1 = img1;
	}
	
	@Length(min=0, max=1024, message="图片2长度不能超过 1024 个字符")
	public String getImg2() {
		return img2;
	}

	public void setImg2(String img2) {
		this.img2 = img2;
	}
	
	@Length(min=0, max=1024, message="ico图片长度不能超过 1024 个字符")
	public String getIco() {
		return ico;
	}

	public void setIco(String ico) {
		this.ico = ico;
	}
	
	@Length(min=0, max=2048, message="活动说明长度不能超过 2048 个字符")
	public String getFlexibledetail() {
		return flexibledetail;
	}

	public void setFlexibledetail(String flexibledetail) {
		this.flexibledetail = flexibledetail;
	}
	
	@Length(min=0, max=1024, message="扩展1长度不能超过 1024 个字符")
	public String getExtend1() {
		return extend1;
	}

	public void setExtend1(String extend1) {
		this.extend1 = extend1;
	}
	
	@Length(min=0, max=255, message="扩展2长度不能超过 255 个字符")
	public String getExtend2() {
		return extend2;
	}

	public void setExtend2(String extend2) {
		this.extend2 = extend2;
	}
	
	@Length(min=0, max=255, message="扩展3长度不能超过 255 个字符")
	public String getExtend3() {
		return extend3;
	}

	public void setExtend3(String extend3) {
		this.extend3 = extend3;
	}
	
	@Length(min=0, max=1024, message="点击之后跳转的路径长度不能超过 1024 个字符")
	public String getDetailurl() {
		return detailurl;
	}

	public void setDetailurl(String detailurl) {
		this.detailurl = detailurl;
	}
	
	public Long getPraisecount() {
		return praisecount;
	}

	public void setPraisecount(Long praisecount) {
		this.praisecount = praisecount;
	}
	
	public Long getSpitslotcount() {
		return spitslotcount;
	}

	public void setSpitslotcount(Long spitslotcount) {
		this.spitslotcount = spitslotcount;
	}
	
	public Long getCommentcount() {
		return commentcount;
	}

	public void setCommentcount(Long commentcount) {
		this.commentcount = commentcount;
	}
	
	public List<TflexibleJoinin2> getTflexibleJoinin2List() {
		return tflexibleJoinin2List;
	}

	public void setTflexibleJoinin2List(List<TflexibleJoinin2> tflexibleJoinin2List) {
		this.tflexibleJoinin2List = tflexibleJoinin2List;
	}
	
}