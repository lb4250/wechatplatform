/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_flexible_joinin3.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.t_flexible_joinin3.entity.TflexibleJoinin3;
import com.jeesite.modules.t_flexible_joinin3.service.TflexibleJoininService3;

/**
 * 活动上传的文件Controller
 * @author zwz
 * @version 2018-10-21
 */
@Controller
@RequestMapping(value = "${adminPath}/t_flexible_joinin3/tflexibleJoinin")
public class TflexibleJoininController3 extends BaseController {

	@Autowired
	private TflexibleJoininService3 tflexibleJoininService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public TflexibleJoinin3 get(String joininid, boolean isNewRecord) {
		return tflexibleJoininService.get(joininid, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("t_flexible_joinin3:tflexibleJoinin:view")
	@RequestMapping(value = {"list", ""})
	public String list(TflexibleJoinin3 tflexibleJoinin, Model model) {
		model.addAttribute("tflexibleJoinin", tflexibleJoinin);
		return "modules/t_flexible_joinin3/tflexibleJoininList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("t_flexible_joinin3:tflexibleJoinin:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<TflexibleJoinin3> listData(TflexibleJoinin3 tflexibleJoinin, HttpServletRequest request, HttpServletResponse response) {
		tflexibleJoinin.setPage(new Page<>(request, response));
		Page<TflexibleJoinin3> page = tflexibleJoininService.findPage(tflexibleJoinin); 
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("t_flexible_joinin3:tflexibleJoinin:view")
	@RequestMapping(value = "form")
	public String form(TflexibleJoinin3 tflexibleJoinin, Model model) {
		model.addAttribute("tflexibleJoinin", tflexibleJoinin);
		return "modules/t_flexible_joinin3/tflexibleJoininForm";
	}

	/**
	 * 保存活动上传的文件
	 */
	@RequiresPermissions("t_flexible_joinin3:tflexibleJoinin:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated TflexibleJoinin3 tflexibleJoinin) {
		tflexibleJoininService.save(tflexibleJoinin);
		return renderResult(Global.TRUE, text("保存活动上传的文件成功！"));
	}
	
	/**
	 * 停用活动上传的文件
	 */
	@RequiresPermissions("t_flexible_joinin3:tflexibleJoinin:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(TflexibleJoinin3 tflexibleJoinin) {
		tflexibleJoinin.setStatus(TflexibleJoinin3.STATUS_DISABLE);
		tflexibleJoininService.updateStatus(tflexibleJoinin);
		return renderResult(Global.TRUE, text("停用活动上传的文件成功"));
	}
	
	/**
	 * 启用活动上传的文件
	 */
	@RequiresPermissions("t_flexible_joinin3:tflexibleJoinin:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(TflexibleJoinin3 tflexibleJoinin) {
		tflexibleJoinin.setStatus(TflexibleJoinin3.STATUS_NORMAL);
		tflexibleJoininService.updateStatus(tflexibleJoinin);
		return renderResult(Global.TRUE, text("启用活动上传的文件成功"));
	}
	
	/**
	 * 删除活动上传的文件
	 */
	@RequiresPermissions("t_flexible_joinin3:tflexibleJoinin:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(TflexibleJoinin3 tflexibleJoinin) {
		tflexibleJoininService.delete(tflexibleJoinin);
		return renderResult(Global.TRUE, text("删除活动上传的文件成功！"));
	}
	
}