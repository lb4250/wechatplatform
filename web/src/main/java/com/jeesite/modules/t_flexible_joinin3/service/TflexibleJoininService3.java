/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_flexible_joinin3.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.t_flexible_joinin3.entity.TflexibleJoinin3;
import com.jeesite.modules.t_flexible_joinin3.dao.TflexibleJoininDao3;
import com.jeesite.modules.file.utils.FileUploadUtils;

/**
 * 活动上传的文件Service
 * @author zwz
 * @version 2018-10-21
 */
@Service
@Transactional(readOnly=true)
public class TflexibleJoininService3 extends CrudService<TflexibleJoininDao3, TflexibleJoinin3> {
	
	/**
	 * 获取单条数据
	 * @param tflexibleJoinin
	 * @return
	 */
	@Override
	public TflexibleJoinin3 get(TflexibleJoinin3 tflexibleJoinin) {
		return super.get(tflexibleJoinin);
	}
	
	/**
	 * 查询分页数据
	 * @param tflexibleJoinin 查询条件
	 * @param tflexibleJoinin.page 分页对象
	 * @return
	 */
	@Override
	public Page<TflexibleJoinin3> findPage(TflexibleJoinin3 tflexibleJoinin) {
		return super.findPage(tflexibleJoinin);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param tflexibleJoinin
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(TflexibleJoinin3 tflexibleJoinin) {
		super.save(tflexibleJoinin);
		// 保存上传图片
		FileUploadUtils.saveFileUpload(tflexibleJoinin.getId(), "tflexibleJoinin_image");
		// 保存上传附件
		FileUploadUtils.saveFileUpload(tflexibleJoinin.getId(), "tflexibleJoinin_file");
	}
	
	/**
	 * 更新状态
	 * @param tflexibleJoinin
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(TflexibleJoinin3 tflexibleJoinin) {
		super.updateStatus(tflexibleJoinin);
	}
	
	/**
	 * 删除数据
	 * @param tflexibleJoinin
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(TflexibleJoinin3 tflexibleJoinin) {
		super.delete(tflexibleJoinin);
	}
	
}