/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_csii_headline.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.t_collect_information.entity.TcollectInformation;
import com.jeesite.modules.t_csii_headline.entity.TcsiiHeadline;
import com.jeesite.modules.t_csii_headline.service.TcsiiHeadlineService;

/**
 * 科蓝头条Controller
 * @author zwz
 * @version 2018-10-23
 */
@Controller
@RequestMapping(value = "${adminPath}/t_csii_headline/tcsiiHeadline")
public class TcsiiHeadlineController extends BaseController {

	@Autowired
	private TcsiiHeadlineService tcsiiHeadlineService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public TcsiiHeadline get(String headlineid, boolean isNewRecord) {
		return tcsiiHeadlineService.get(headlineid, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("t_csii_headline:tcsiiHeadline:view")
	@RequestMapping(value = {"list", ""})
	public String list(TcsiiHeadline tcsiiHeadline, Model model) {
		model.addAttribute("tcsiiHeadline", tcsiiHeadline);
		return "modules/t_csii_headline/tcsiiHeadlineList";
	}
	
	/**
	 * 查询列表数据
	 */
//	@RequiresPermissions("t_csii_headline:tcsiiHeadline:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<TcsiiHeadline> listData(TcsiiHeadline tcsiiHeadline, HttpServletRequest request, HttpServletResponse response) {
		
		String pageNo = request.getParameter("pageNo");
		String pageSize = request.getParameter("pageSize");
		
		Page<TcsiiHeadline> pg = new Page<TcsiiHeadline>(request, response);
		pg.setPageNo(  ("".equals( pageNo )|| pageNo==null)? 1 : Integer.parseInt(request.getParameter("pageNo") ) );
		pg.setPageSize(  ( "".equals(pageSize) || pageSize==null )? 10 : Integer.parseInt( request.getParameter("pageSize")   ));
		
		Page<TcsiiHeadline> page = tcsiiHeadlineService.findPage( pg, tcsiiHeadline); 
		return page;	
	}
	
	
	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("t_csii_headline:tcsiiHeadline:view")
	@RequestMapping(value = "form")
	public String form(TcsiiHeadline tcsiiHeadline, Model model) {
		model.addAttribute("tcsiiHeadline", tcsiiHeadline);
		return "modules/t_csii_headline/tcsiiHeadlineForm";
	}

	/**
	 * 保存科蓝头条(sp)
	 */
	@RequiresPermissions("t_csii_headline:tcsiiHeadline:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated TcsiiHeadline tcsiiHeadline) {
		tcsiiHeadlineService.save(tcsiiHeadline);
		return renderResult(Global.TRUE, text("保存科蓝头条(sp)成功！"));
	}
	
	/**
	 * 停用科蓝头条(sp)
	 */
	@RequiresPermissions("t_csii_headline:tcsiiHeadline:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(TcsiiHeadline tcsiiHeadline) {
		tcsiiHeadline.setStatus(TcsiiHeadline.STATUS_DISABLE);
		tcsiiHeadlineService.updateStatus(tcsiiHeadline);
		return renderResult(Global.TRUE, text("停用科蓝头条(sp)成功"));
	}
	
	/**
	 * 启用科蓝头条(sp)
	 */
	@RequiresPermissions("t_csii_headline:tcsiiHeadline:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(TcsiiHeadline tcsiiHeadline) {
		tcsiiHeadline.setStatus(TcsiiHeadline.STATUS_NORMAL);
		tcsiiHeadlineService.updateStatus(tcsiiHeadline);
		return renderResult(Global.TRUE, text("启用科蓝头条(sp)成功"));
	}
	
	/**
	 * 删除科蓝头条(sp)
	 */
	@RequiresPermissions("t_csii_headline:tcsiiHeadline:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(TcsiiHeadline tcsiiHeadline) {
		tcsiiHeadlineService.delete(tcsiiHeadline);
		return renderResult(Global.TRUE, text("删除科蓝头条(sp)成功！"));
	}
	
}