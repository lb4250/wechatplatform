/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_indexbanner.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.t_indexbanner.entity.Tindexbanner;

/**
 * 首页bannerDAO接口
 * @author zwz
 * @version 2018-10-12
 */
@MyBatisDao
public interface TindexbannerDao extends CrudDao<Tindexbanner> {
	
}