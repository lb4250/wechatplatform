/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_indexbanner.entity;

import org.hibernate.validator.constraints.Length;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 首页bannerEntity
 * @author zwz
 * @version 2018-10-12
 */
@Table(name="t_indexbanner", alias="a", columns={
		@Column(name="bannerid", attrName="bannerid", label="banner 的主键id", isPK=true),
		@Column(name="bannername", attrName="bannername", label="banner的名称"),
		@Column(name="detailurl", attrName="detailurl", label="点击之后跳转的链接"),
		@Column(name="img", attrName="img", label="图片0"),
		@Column(name="img1", attrName="img1", label="图片1"),
		@Column(name="img2", attrName="img2", label="图片2"),
		@Column(name="videourl", attrName="videourl", label="视频路径"),
		@Column(name="extend1", attrName="extend1", label="扩展1"),
		@Column(name="extend2", attrName="extend2", label="扩展2"),
		@Column(name="extend3", attrName="extend3", label="扩展3"),
		@Column(name="bannercontent", attrName="bannercontent", label="文字内容"),
		@Column(includeEntity=DataEntity.class),
	}, orderBy="a.update_date DESC"
)
public class Tindexbanner extends DataEntity<Tindexbanner> {
	
	private static final long serialVersionUID = 1L;
	private String bannerid;		// banner 的主键id
	private String bannername;		// banner的名称
	private String detailurl;		// 点击之后跳转的链接
	private String img;		// 图片0
	private String img1;		// 图片1
	private String img2;		// 图片2
	private String videourl;		// 视频路径
	private String extend1;		// 扩展1
	private String extend2;		// 扩展2
	private String extend3;		// 扩展3
	private String bannercontent;		// 文字内容
	
	public Tindexbanner() {
		this(null);
	}

	public Tindexbanner(String id){
		super(id);
	}
	
	public String getBannerid() {
		return bannerid;
	}

	public void setBannerid(String bannerid) {
		this.bannerid = bannerid;
	}
	
	@Length(min=0, max=255, message="banner的名称长度不能超过 255 个字符")
	public String getBannername() {
		return bannername;
	}

	public void setBannername(String bannername) {
		this.bannername = bannername;
	}
	
	@Length(min=0, max=1024, message="点击之后跳转的链接长度不能超过 1024 个字符")
	public String getDetailurl() {
		return detailurl;
	}

	public void setDetailurl(String detailurl) {
		this.detailurl = detailurl;
	}
	
	@Length(min=0, max=1024, message="图片0长度不能超过 1024 个字符")
	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}
	
	@Length(min=0, max=1024, message="图片1长度不能超过 1024 个字符")
	public String getImg1() {
		return img1;
	}

	public void setImg1(String img1) {
		this.img1 = img1;
	}
	
	@Length(min=0, max=1024, message="图片2长度不能超过 1024 个字符")
	public String getImg2() {
		return img2;
	}

	public void setImg2(String img2) {
		this.img2 = img2;
	}
	
	@Length(min=0, max=1024, message="视频路径长度不能超过 1024 个字符")
	public String getVideourl() {
		return videourl;
	}

	public void setVideourl(String videourl) {
		this.videourl = videourl;
	}
	
	@Length(min=0, max=255, message="扩展1长度不能超过 255 个字符")
	public String getExtend1() {
		return extend1;
	}

	public void setExtend1(String extend1) {
		this.extend1 = extend1;
	}
	
	@Length(min=0, max=255, message="扩展2长度不能超过 255 个字符")
	public String getExtend2() {
		return extend2;
	}

	public void setExtend2(String extend2) {
		this.extend2 = extend2;
	}
	
	@Length(min=0, max=255, message="扩展3长度不能超过 255 个字符")
	public String getExtend3() {
		return extend3;
	}

	public void setExtend3(String extend3) {
		this.extend3 = extend3;
	}
	
	@Length(min=0, max=255, message="文字内容长度不能超过 255 个字符")
	public String getBannercontent() {
		return bannercontent;
	}

	public void setBannercontent(String bannercontent) {
		this.bannercontent = bannercontent;
	}
	
}