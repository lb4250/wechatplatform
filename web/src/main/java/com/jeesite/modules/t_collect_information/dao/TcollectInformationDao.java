/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_collect_information.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.t_collect_information.entity.TcollectInformation;

/**
 * 我收藏的资讯DAO接口
 * @author zwz
 * @version 2018-10-19
 */
@MyBatisDao
public interface TcollectInformationDao extends CrudDao<TcollectInformation> {
	
}